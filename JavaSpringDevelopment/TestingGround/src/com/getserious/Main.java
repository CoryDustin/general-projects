package com.getserious;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args)
    {
	    System.out.println(countPairsNew(new int[] {5, 5, 3, 4}, 10));
    }

    public static int countPairsNew(int[] pairArray, int match)
    {
        int count = 0;
        // Use Hash map for linear look up time
        Map<Integer, Integer> tempMap = new HashMap<>();

        // This is now O(n) linear complexity
        for(int i = 0; i < pairArray.length; i++)
        {
            // Subtract the number we are looking for by the current indexed number
            // in the pairArray, this will give us the number we are about
            int numLookingFor = match - pairArray[i];
            if(tempMap.containsKey(numLookingFor))
            {
                count++;
                // If we have found the number we are looking for remove it from
                // The map so we don't have duplicate entries
                tempMap.remove(i);
            }
            else
            {
                tempMap.put(pairArray[i], 0);
            }
        }
        return count;
    }

    /**
     * Complexity would be: O(n^2)
     *
     * countPairs returns the number of pairs
     * of integers in a that add up to x.
     * Given a = [1,2,3,4] and x = 5, countPairs
     * should return 2. This is because
     * the sum of [1,4] is 5 and the sum of [2,3]
     * is 5 and those are the only two
     * pairs that add up to 5.
     */
    public static int countPairs(int[] a, int x)
    {
        int count = 0;
        for(int i = 0; i < a.length; i++)
        {
            // Corrected inner loop to start from the current index + 1,
            // this will ensure we are checking all numbers after the
            // current index we are on.
            for(int j = i + 1; j < a.length; j++)
            {
                // Corrected if statement to actually use the array it is
                // iterating over and not just the index
                if (a[i] + a[j] == x)
                {
                    count ++;
                }
            }
        }
        return count;
    }
    
    //{0, 1, 2}
    //{1, 0, 1, 2}
    // Will correctly see if the first array is within the second array
    public static boolean compareArray(int[] arr1, int[] arr2)
    {
        int count = 0;
        int prevIndex = 0;
        if(arr1.length > arr2.length)
            return false;

        for(int i = 0; i < arr1.length; i++)
        {
            for(; prevIndex < arr2.length;)
            {
                if(arr1[i] == arr2[prevIndex])
                {
                    count++;
                    prevIndex++;
                    break;
                }
                else
                {
                    count = 0;
                    prevIndex++;
                    if(i != 0)
                        break;
                }
            }
        }
        return count == arr1.length;
    }
}
