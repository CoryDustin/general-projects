package com.getserious.blogger.security;

import com.getserious.blogger.model.UserProfile;
import com.getserious.blogger.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.getserious.blogger.model.User;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;

@Service("customUserDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService
{
    @Autowired
    private UserService userService;

    @Override
    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = userService.findByUsername(username);
        if(user == null)
            throw new UsernameNotFoundException("Username was not found!");

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.getState().equals("Active"), true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user)
    {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for(UserProfile userProfile : user.getUserProfiles())
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + userProfile.getType()));
        return grantedAuthorities;
    }
}
