package com.getserious.blogger.model.service;


import com.getserious.blogger.model.User;
import com.getserious.blogger.model.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional
public class UserService
{
    @Autowired
    private UserDao userDao;

    public User findById(int id)
    {
        return userDao.findById(id);
    }

    public User findByUsername(String username)
    {
        return userDao.findByUsername(username);
    }



}
