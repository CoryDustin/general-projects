package com.getserious.blogger.model.dao;

import com.getserious.blogger.model.User;

public interface UserDao
{
    User findById(int id);

    User findByUsername(String username);

}
