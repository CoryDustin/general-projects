package com.getserious.blogger.model.enums;


public enum UserProfileType
{
    USER("USER"),
    DBA("DBA"),
    ADMIN("ADMIN");

    private String userProfileType;

    private UserProfileType(String userProfileType)
    {
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType()
    {
        return this.userProfileType;
    }
}
