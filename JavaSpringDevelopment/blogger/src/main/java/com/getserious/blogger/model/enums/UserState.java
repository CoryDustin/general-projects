package com.getserious.blogger.model.enums;


public enum UserState
{
    ACTIVE("Active"),
    DISABLED("Disabled"),
    DELETED("Deleted"),
    LOCKED("Locked");

    private String state;

    private UserState(String state)
    {
        this.state = state;
    }

    public String getState()
    {
        return this.state;
    }

    @Override
    public String toString()
    {
        return this.state;
    }

    public String getName()
    {
        return this.name();
    }

}
