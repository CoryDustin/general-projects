package com.getserious.giflib.data;


import com.getserious.giflib.model.Gif;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class GifRepository
{
    private static final List<Gif> ALL_GIFS = Arrays.asList(
        new Gif("android-explosion", 1, LocalDate.of(2016, 03, 13), "Cory Gonzales", true),
        new Gif("ben-and-mike", 2, LocalDate.of(2016, 03, 13), "Jennifer Agosto", false),
        new Gif("book-dominos", 3, LocalDate.of(2016, 03, 13), "John Smith", true),
        new Gif("compiler-bot", 3, LocalDate.of(2016, 03, 13), "Apple Jacks", false),
        new Gif("cowboy-coder", 1, LocalDate.of(2016, 03, 13), "Cory Gonzales", true),
        new Gif("infinite-andrew", 2, LocalDate.of(2016, 03, 13), "Cory Gonzales", false)
    );

    public Gif findByName(String name)
    {
        for(Gif gif : ALL_GIFS)
        {
            if(gif.getName().equals(name))
                return gif;
        }
        return null;
    }

    public List<Gif> getAllGifs()
    {
        return ALL_GIFS;
    }

    public List<Gif> findByCategoryId(int id)
    {
        List<Gif> resultList = new ArrayList<>();

        // Could be accomplished with the following lambda in Java 8
        //List<Gif> resultList = ALL_GIFS.stream().filter(gif -> gif.getCategoryId() == id).collect(Collectors.toList());
        for(Gif gif : ALL_GIFS)
        {
            if(gif.getCategoryId() == id)
                resultList.add(gif);
        }
        return resultList;
    }
}
