package com.getserious.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false) // ignore this repository from the rest API
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

}
