package com.getserious.core;

import javax.persistence.*;

@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private final Long id;
    @Version
    private Long version; // Will add an ETag to the headers to let the requester know if things have changed with this Entity

    protected BaseEntity(){
        id = null;
    }

}
