package com.getserious.ignrestful.model;

/**
 * IGN Request object that will be used to model request params
 * to the IGN servers.
 */
public class IgnRequest
{
    private int startIndex;
    private int count;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
