package com.getserious.ignrestful.controller;

import com.getserious.ignrestful.model.IgnData;
import com.getserious.ignrestful.model.IgnRequest;
import com.getserious.ignrestful.model.IgnResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class MainController
{
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexGet(Model model)
    {
        model.addAttribute("ignRequest", new IgnRequest());
        return "index";
    }

    @RequestMapping(value = "/getIgnFeed", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public IgnResponse getIgnFeed(@RequestBody IgnRequest ignRequest)
    {
        System.out.println("Start: " + ignRequest.getStartIndex() + " Count: " + ignRequest.getCount());
        RestTemplate restTemplate = new RestTemplate();
        IgnResponse response = restTemplate.getForObject("http://ign-apis.herokuapp.com/articles?startIndex=" +
                                                    ignRequest.getStartIndex() + "&count=" + ignRequest.getCount(), IgnResponse.class);
        return response;
    }

}
