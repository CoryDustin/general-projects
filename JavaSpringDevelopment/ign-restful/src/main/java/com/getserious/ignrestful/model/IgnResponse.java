package com.getserious.ignrestful.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.json.JSONArray;
import java.io.IOException;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IgnResponse
{
    private long startIndex;
    private long count;
    private IgnData[] data;

//    public IgnResponse(String responseUrl)
//    {
//        this.startIndex = 0;
//        this.count = 0;
//        this.data = new ArrayList<>();
//        this.constructFromUrl(responseUrl);
//    }

    public IgnResponse() {}

//    private void constructFromUrl(String responseUrl)
//    {
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder().url(responseUrl).build();
//        try {
//            Response response = client.newCall(request).execute();
//            JSONObject jsonObj = new JSONObject(response.body().string());
//            this.startIndex = jsonObj.getInt("startIndex");
//            this.count = jsonObj.getInt("count");
//            JSONArray jsonArray = jsonObj.getJSONArray("data");
//            for(int i = 0; i < jsonArray.length(); i++)
//            {
//                JSONObject dataJson = jsonArray.getJSONObject(i);
//                JSONObject metaDataJson = dataJson.getJSONObject("metadata");
//
//                String [] networks;
//                JSONArray networkJsonArray = metaDataJson.getJSONArray("networks");
//                networks = new String[networkJsonArray.length()];
//
//                for(int j = 0; j < networkJsonArray.length(); j++)
//                    networks[i] = networkJsonArray.getString(i);
//
//                IgnData data = new IgnData(dataJson.getString("thumbnail"),
//                                        new IgnMetaData(metaDataJson.getString("headline"),
//                                        networks,
//                                        metaDataJson.getString("state"),
//                                        metaDataJson.getString("slug"),
//                                        metaDataJson.getString("subHeadline"),
//                                        metaDataJson.getString("publishDate"),
//                                        metaDataJson.getString("articleType")));
//                this.data.add(data);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    public long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(long startIndex) {
        this.startIndex = startIndex;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public IgnData[] getData() {
        return data;
    }

    public void setData(IgnData[] data) {
        this.data = data;
    }

    public String toString()
    {
        String result = "Response: \n\tStartIndex: " + this.startIndex + "\n\tCount: " + this.count +
                "\n\tData: ";

        for(IgnData dataItem : this.data)
        {
            result += "\t\t" + dataItem.toString();
        }
        return result;
    }
}
