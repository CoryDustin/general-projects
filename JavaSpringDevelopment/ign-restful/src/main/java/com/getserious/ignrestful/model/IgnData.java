package com.getserious.ignrestful.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IgnData
{
    private String thumbnail;
    private IgnMetaData metadata;

    public IgnData(){}

    public IgnData(String thumbnail, IgnMetaData metadata)
    {
        this.thumbnail = thumbnail;
        this.metadata = metadata;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public IgnMetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(IgnMetaData metadata) {
        this.metadata = metadata;
    }

    public String toString()
    {
        return "Thumbnail: " + this.thumbnail + "\n" + "MetaData: " + this.metadata.toString();
    }
}