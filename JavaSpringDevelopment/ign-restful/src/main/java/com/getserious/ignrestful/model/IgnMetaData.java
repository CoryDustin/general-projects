package com.getserious.ignrestful.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IgnMetaData
{
    private String headline;
    private String[] networks;
    private String state;
    private String slug;
    private String subHeadline;
    private String publishDate;
    private String articleType;

    public IgnMetaData(){}

    public IgnMetaData(String headline, String[] networks, String state, String slug,
                       String subHeadline, String publishDate, String articleType)
    {
        this.headline = headline;
        this.networks = networks;
        this.state = state;
        this.slug = slug;
        this.subHeadline = subHeadline;
        this.publishDate = publishDate;
        this.articleType = articleType;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String[] getNetworks() {
        return networks;
    }

    public void setNetworks(String[] networks) {
        this.networks = networks;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSubHeadline() {
        return subHeadline;
    }

    public void setSubHeadline(String subHeadline) {
        this.subHeadline = subHeadline;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }
}
