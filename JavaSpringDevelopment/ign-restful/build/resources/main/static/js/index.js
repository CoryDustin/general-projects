$(function() {
    var $loadingImage = $("#loading-img");
    var $feedDisplay = $("#feed-display");

    $("#submit-button").click(function () {
        $feedDisplay.empty();
        $loadingImage.removeClass("hidden");
        var ignRequest = {
            "startIndex": $("#start-index-textinput").val(),
            "count": $("#count-textinput").val()
        };
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf8",
            dataType: "json",
            url: "/getIgnFeed",
            data: JSON.stringify(ignRequest),
            success: function (result) {
                $loadingImage.addClass("hidden");
                populateFeedTable(result);
            },
            fail: function () {
                $loadingImage.addClass("hidden");
                $feedDisplay.html("<h5>There was an issue getting the IGN Feed content requested!</h5>");
            }
        })
    });
});

/**
 * Populates the feed table to display based on the ignResponseObj values
 * @param {Object} ignResposeObj: The JSON Object that is returned from the AJAX request to gather the feed
 */
function populateFeedTable(ignResposeObj)
{
    if (!ignResposeObj.hasOwnProperty('count'))
        return;

    for (var i = 0; i < ignResposeObj['count']; i++)
    {
        var data = ignResposeObj['data'][i];
        var thumbnail = data['thumbnail'];
        var metadata = data['metadata'];

        var feedDisplayHtml = '<tr class="feed-row">' +
            '<td class="center">' + (i+1) + '</td>' +
            '<td class="center"><p>' + metadata['headline'] + '<br/>';

        // We have to account for the possibility of there not being a
        // subHeadline in the returned feed data
        if(metadata['subHeadline'] != null)
            feedDisplayHtml += truncateString(metadata['subHeadline'], 30) + '</p></td>';
        else
            feedDisplayHtml += '</p></td>';
        feedDisplayHtml += '<td class="center">' +
            getTimeFormattedString(metadata['publishDate']) + '</td></tr>';

        $newFeedRow = $(feedDisplayHtml).appendTo("#feed-display");

        console.log("Thumbnail: " + thumbnail);

        // Creating function closure to gain access to each thumbnail needed
        // within the inner hover enter/leave functions
        (function(innerThumbnail) {
            $newFeedRow.hover(function () {
                $(this).css("background-image", 'url(' + innerThumbnail + ')');
                $(this).find("td").css("color", "white");
            }, function () {
                $(this).css("background-image", "");
                $(this).find("td").css("color", "black");
            });
        })(thumbnail);
    }
}

/**
 * Helper function that will truncate a string and append "..." at the
 * end based on if the string is longer than the maxLength param
 * @param {string} stringToTuncate
 * @param {Number} maxLength
 * @returns {string} - The new truncated string
 */
function truncateString(stringToTuncate, maxLength)
{
    if(stringToTuncate.length > maxLength)
        stringToTuncate = stringToTuncate.substring(0, maxLength) + "...";

    return stringToTuncate;
}

/**
 * Helper function that will take an ISO formatted time string and retrieve
 * the current time from that string ie -> "8:23"
 * @param {string} isoTimeString - The ISO formatted string to parse
 * @returns {string} - The new formatted time string
 */
function getTimeFormattedString(isoTimeString)
{
    var timeStr = "";
    try
    {
        date = new Date(isoTimeString);

        var minutes = addZero(date.getMinutes());
        var hours = parseInt(date.getHours());
        // Convert time from military to standard time
        hours = ((hours + 11) % 12) + 1;
        hours = addZero(hours);

        timeStr = hours + ":" + minutes;
    }
    catch(e)
    {
        console.log("ERROR: " + e);
    }
    return timeStr;
}

/**
 * Adds a zero to the number and returns as a string representation of that number ie 9 = "09"
 * @param {Number} number - The number to add zero and convert to string
 * @returns {string}
 */
function addZero(number) {
    if (number < 10) {
        number = "0" + number;
    }
    return number;
}