package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.dao.BlogDao;
import com.getserious.viral_blogger.dao.UserDao;
import com.getserious.viral_blogger.models.Blog;
import com.getserious.viral_blogger.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class BlogServiceImpl implements BlogService
{
    @Autowired BlogDao blogDao;
    @Autowired UserDao userDao;

    @Override
    public Blog findBlogById(int id)
    {
        Blog blog;
        try
        {
            blog = blogDao.findById(id);
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in BlogServiceImpl:findBlogById(String username):" + e.toString());
            blog = null;
        }
        return blog;
    }

    @Override
    public List<Blog> findAllBlogs()
    {
        List<Blog> blogs = new ArrayList<>();
        try
        {
            for(Blog blog : blogDao.findAll()){
                blogs.add(blog);
            }
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in BlogServiceImpl:findAllBlogs():" + e.toString());
            blogs = null;
        }
        return blogs;
    }

    @Override
    public Set<Blog> findAllBlogsByUsername(String username)
    {
        Set<Blog> allBlogs;
        try
        {
            User user = userDao.findByUsername(username);
            allBlogs = user.getPastBlogs();
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in BlogServiceImpl:findAllBlogsByUsername(String username):" + e.toString());
            allBlogs = null;
        }
        return allBlogs;
    }

    @Override
    public Blog saveBlog(Blog blog) {
        return blogDao.save(blog);
    }

}
