package com.getserious.viral_blogger.models;

import com.getserious.viral_blogger.enums.UserRoleType;

import javax.persistence.*;

@Entity
@Table(name="USER_ROLES")
public class UserRole
{
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = "ROLE_" + name.toUpperCase();
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
