package com.getserious.viral_blogger.exceptions;

/**
 * Class that can be used to handle specific exceptions
 * related to model/database errors such as duplicate entries
 */
public class BloggerDataException extends Exception
{

    private String message;

    /**
     * Class constructor that specifies a message to be added to
     * the exceptions stack trace and toString
     *
     * @param message The message to pass to the exception
     */
    public BloggerDataException(String message)
    {
        super(message);
        this.message = message;
    }

    /**
     * Class constructor that specifies a Throwable object
     *
     * @param cause The throwable object
     */
    public BloggerDataException(Throwable cause)
    {
        super(cause);
        this.message = "";
    }

    /**
     * Class constructor that specifies a message and Throwable
     * object to be used for this exception
     *
     * @param message   The message to pass to the exception
     * @param cause     The throwable object
     */
    public BloggerDataException(String message, Throwable cause)
    {
        super(message, cause);
        this.message = message;
    }

    /**
     * The Class constructor that specifies a message, Throwable
     * object, flag for enabling Suppression, and flag for making the
     * stack trace writable
     *
     * @param message            The message to pass to the exception
     * @param cause              The throwable object
     * @param enableSuppression  Expression indicating whether to enable suppression or not
     * @param writableStackTrace Expression indicating if the stack trace should be writable
     */
    public BloggerDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        this.message = message;
    }

    /**
     * The toString representing this object
     *
     * @return The string representation of a BloggerDataException
     */
    @Override
    public String toString() {
        return message;
    }
}
