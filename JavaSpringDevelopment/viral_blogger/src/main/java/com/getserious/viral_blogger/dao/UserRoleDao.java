package com.getserious.viral_blogger.dao;

import com.getserious.viral_blogger.models.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleDao extends CrudRepository<UserRole, Long>
{
    @Query("select role from UserRole role where role.name=:name")
    UserRole findByRoleName(@Param("name") String name);
}
