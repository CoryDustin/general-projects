package com.getserious.viral_blogger.web;

/**
 * Class that represents a flash message added to a RedirectAttributes object.
 * Will contain the message as well as status for display on the web frontend
 */
public class FlashMessage
{
    private String message;
    private Status status;

    public FlashMessage(String message, Status status)
    {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static enum Status{
        SUCCESS,
        FAILURE
    }

}
