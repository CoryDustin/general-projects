package com.getserious.viral_blogger.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="blog")
public class Blog
{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="BLOG_ID")
    private int id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="USER_ID", nullable=false)
    @JsonBackReference
    private User postedBy;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="blog")
    @JsonManagedReference
    private Set<Comment> comments;

    @Column(name="DATE_POSTED", nullable=false)
    private Date datePosted;

    @Column(name="TITLE", nullable=false)
    private String title;

    @Column(name="CONTENT", nullable=false, length=1024)
    private String content;

    public Blog(){}

    public Blog(Date datePosted, User postedBy, String title, String content)
    {
        this.datePosted = datePosted;
        this.postedBy = postedBy;
        this.title = title;
        this.content = content;
        this.comments = new HashSet<>(0);
    }

    public Blog(User postedBy, String title, String content)
    {
        this.postedBy = postedBy;
        this.title = title;
        this.content = content;
        this.comments = new HashSet<>(0);
        this.datePosted = new Date();
    }

//    @PrePersist
//    protected void onCreate()
//    {
//        this.datePosted = new Date();
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

    public User getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(User postedBy) {
        this.postedBy = postedBy;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", datePosted=" + datePosted +
                ", postedBy=" + postedBy +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
