package com.getserious.viral_blogger.web.controller;

import com.getserious.viral_blogger.models.User;
import com.getserious.viral_blogger.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.getserious.viral_blogger.web.FlashMessage;

import java.io.IOException;

@Controller
public class ProfileController
{
    @Autowired
    UserService userService;

    // The main profile page
    @RequestMapping(value="/profile", method=RequestMethod.GET)
    public String profilePage(ModelMap modelMap, @AuthenticationPrincipal User user)
    {
        modelMap.put("user", user);
        return "profile";
    }

    // Upload new profile icon
    @RequestMapping(value="/profile/uploadpic", method=RequestMethod.POST)
    public String uploadProfileIcon(@RequestParam MultipartFile file,
                                    @AuthenticationPrincipal User user,
                                    RedirectAttributes redirectAttributes)
    {
        try{
            user.setProfilePicture(file.getBytes());
            userService.saveUser(user);
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Updated your profile icon!",
                                                                           FlashMessage.Status.SUCCESS));
        }catch(IOException e)
        {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("There was an upload issue, please try again",
                                                                           FlashMessage.Status.FAILURE));
        }

        return "redirect:/profile";
    }


}
