package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.dao.UserDao;
import com.getserious.viral_blogger.dao.UserRoleDao;
import com.getserious.viral_blogger.enums.UserRoleType;
import com.getserious.viral_blogger.exceptions.BloggerDataException;
import com.getserious.viral_blogger.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired private UserDao userDao;
    @Autowired private UserRoleDao userRoleDao;

    @Override
    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = findUserByUsername(username);
        if(user == null)
            throw new UsernameNotFoundException("Username was not found!");

        return user;
    }

    @Override
    public User findUserById(int id) {
        User user;
        try
        {
            user = userDao.findById(id);
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in UserServiceImpl:findUserById(int id):" + e.toString());
            user = null;
        }
        return user;
    }

    @Override
    public User findUserByUsername(String username)
    {
        User user;
        try
        {
            user = userDao.findByUsername(username);
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in UserServiceImpl:findUserByUsername(String username):" + e.toString());
            user = null;
        }
        return user;
    }

    @Override
    public List<User> findAllUsers()
    {
        List<User> users = new ArrayList<>();
        try
        {
            for(User user : userDao.findAll()){
                users.add(user);
            }
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in UserServiceImpl:findAllBlogs():" + e.toString());
            users = null;
        }
        return users;
    }

    @Override
    public User saveNewUser(User user) throws BloggerDataException
    {
        // Check to see if the database contains a user with the
        // specified username or email, if so we cannot add and throw exception
        for(User savedUser : findAllUsers())
        {
            if(savedUser.getUsername().equals(user.getUsername()))
                throw new BloggerDataException("That user name is already taken");
            else if(savedUser.getEmail().equals(user.getEmail()))
                throw new BloggerDataException("That email is already associated with an account");
        }

        // Hash password with BCrypt encoder
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        //TODO: Need to give ability for current admins to change any user roles
        user.setRole(userRoleDao.findByRoleName(UserRoleType.USER.getUserRoleType()));

        return userDao.save(user);
    }

    @Override
    public User saveUser(User user)
    {
        return userDao.save(user);
    }



    private List<GrantedAuthority> getGrantedAuthority(User user)
    {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().getName()));
        return authorities;
    }
}
