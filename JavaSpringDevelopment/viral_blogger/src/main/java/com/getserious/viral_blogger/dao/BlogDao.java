package com.getserious.viral_blogger.dao;

import com.getserious.viral_blogger.models.Blog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BlogDao extends CrudRepository<Blog, Long>
{
    Blog findById(int id);
}
