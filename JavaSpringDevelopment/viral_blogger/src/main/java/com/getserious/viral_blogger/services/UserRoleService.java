package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.models.UserRole;

/**
 * Created by cgonzales on 6/29/2016.
 */
public interface UserRoleService
{
    UserRole findByRoleName(String name);
}
