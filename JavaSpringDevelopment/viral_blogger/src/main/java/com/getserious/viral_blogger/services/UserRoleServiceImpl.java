package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.dao.UserRoleDao;
import com.getserious.viral_blogger.models.UserRole;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by cgonzales on 6/29/2016.
 */
public class UserRoleServiceImpl implements UserRoleService
{
    @Autowired UserRoleDao userRoleDao;

    @Override
    public UserRole findByRoleName(String name) {
        return userRoleDao.findByRoleName(name);
    }
}
