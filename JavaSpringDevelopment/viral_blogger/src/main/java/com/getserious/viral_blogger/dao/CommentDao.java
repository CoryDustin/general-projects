package com.getserious.viral_blogger.dao;


import com.getserious.viral_blogger.models.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentDao extends CrudRepository<Comment, Long>
{
    Comment findById(int id);
}
