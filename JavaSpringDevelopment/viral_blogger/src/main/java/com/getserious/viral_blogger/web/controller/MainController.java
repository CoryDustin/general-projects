package com.getserious.viral_blogger.web.controller;

import com.getserious.viral_blogger.models.Blog;
import com.getserious.viral_blogger.models.User;
import com.getserious.viral_blogger.services.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@Controller
public class MainController
{
    @Autowired
    BlogService blogService;

    @RequestMapping("/")
    public String index(ModelMap modelMap)
    {
        List<Blog> allBlogs = blogService.findAllBlogs();
        if(!allBlogs.isEmpty())
            modelMap.put("blogPosts", allBlogs);

        return "index";
    }

    @RequestMapping("/login")
    public String login()
    {
        return "login";
    }

    @RequestMapping("/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null)
            new SecurityContextLogoutHandler().logout(request, response, auth);
        return "redirect:/login?logout";
    }


}
