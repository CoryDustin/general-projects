package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.dao.BlogDao;
import com.getserious.viral_blogger.dao.CommentDao;
import com.getserious.viral_blogger.models.Blog;
import com.getserious.viral_blogger.models.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public class CommentServiceImpl implements CommentService
{
    @Autowired private BlogDao blogDao;
    @Autowired private CommentDao commentDao;

    @Override
    public Set<Comment> findAllCommentsByBlogId(int id)
    {
        Set<Comment> comments;
        try
        {
            Blog blog = blogDao.findById(id);
            comments = blog.getComments();
        }
        catch(DataAccessException e)
        {
            System.err.println("Error in CommentServiceImpl:findAllCommentsByBlogId(int id):" + e.toString());
            comments = null;
        }
        return comments;
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentDao.save(comment);
    }

    public Object saveEntity(Object entity)
    {
        Object savedEntity;
        try
        {
            savedEntity = ((CrudRepository)entity).save(entity);
        }
        catch(ClassCastException e)
        {
            System.err.println("Error in CommentServiceImpl:saveEntity(Object entity):" + e.toString());
            savedEntity = null;
        }
        return savedEntity;
    }

}
