package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.models.Comment;

import java.util.Set;

public interface CommentService
{
    Set<Comment> findAllCommentsByBlogId(int id);
    Comment saveComment(Comment comment);
    Object saveEntity(Object object);
}
