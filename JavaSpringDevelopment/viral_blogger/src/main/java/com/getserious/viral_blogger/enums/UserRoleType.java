package com.getserious.viral_blogger.enums;

public enum UserRoleType
{
    USER("ROLE_USER"),
    DBA("ROLE_DBA"),
    ADMIN("ROLE_ADMIN");

    String userRoleType;

    private UserRoleType(String userRoleType)
    {
        this.userRoleType = userRoleType;
    }

    public String getUserRoleType()
    {
        return this.userRoleType;
    }
}
