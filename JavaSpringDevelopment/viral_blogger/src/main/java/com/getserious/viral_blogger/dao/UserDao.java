package com.getserious.viral_blogger.dao;


import com.getserious.viral_blogger.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Long>
{
    User findByUsername(String username);
    User findById(int id);
}
