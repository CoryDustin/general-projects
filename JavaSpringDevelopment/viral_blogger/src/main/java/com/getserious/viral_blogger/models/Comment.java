package com.getserious.viral_blogger.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import java.util.Date;


@Entity
@Table(name="comment")
public class Comment
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="COMMENT_ID")
    private int id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="USER_ID", nullable=false)
    private User commentBy;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="BLOG_ID", nullable=false)
    @JsonBackReference
    private Blog blog;

    @Column(name="DATE_CREATED", nullable=false)
    private Date dateCreated;

    @Column(name="COMMENT_TEXT", nullable=false)
    private String commentText;

    public Comment(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCommentBy() {
        return commentBy;
    }

    public void setCommentBy(User commentBy) {
        this.commentBy = commentBy;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
