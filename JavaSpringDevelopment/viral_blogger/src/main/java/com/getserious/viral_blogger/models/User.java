package com.getserious.viral_blogger.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.getserious.viral_blogger.enums.UserRoleType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="USERS")
public class User implements UserDetails{

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="USER_ID", nullable=false)
    private int id;

    @Column(name="USERNAME", unique=true, nullable=false)
    private String username;

    @Column(name="PASSWORD", nullable=false)
    private String password;

    @Column(name="ENABLED", nullable=false)
    private boolean enabled;

    @Column(name="FIRST_NAME", nullable=false)
    private String firstname;

    @Column(name="LAST_NAME", nullable=false)
    private String lastname;

    @Column(name="EMAIL", unique=true, nullable=false)
    private String email;

    @Lob
    @Column(name="PROFILE_PICTURE")
    private byte[] profilePicture;

    @Column(name="DATE_JOINED", nullable=false)
    private Date dateJoined;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="postedBy")
    @JsonManagedReference
    private Set<Blog> pastBlogs;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="userrole_id")
    private UserRole role;

    public User()
    {
        this.username = "";
        this.password = "";
        this.enabled = true;
        this.firstname = "";
        this.lastname = "";
        this.email = "";
        this.dateJoined = new Date();
        this.pastBlogs = new HashSet<>(0);
        this.role = new UserRole();
    }

    public User(String username, String password, String firstname, String lastname, String email)
    {
        this.username = username;
        this.password = password;
        this.enabled = true;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.dateJoined = new Date();
        this.pastBlogs = new HashSet<>(0);
        this.role = new UserRole();

    }

    public User(String username, String password)
    {
        this.username = username;
        this.password = password;
        this.enabled = true;
        this.firstname = "";
        this.lastname = "";
        this.email = "";
        this.dateJoined = new Date();
        this.pastBlogs = new HashSet<>(0);
        this.role = new UserRole();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role.getName()));
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

//    @JsonIgnore
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Set<Blog> getPastBlogs() {
        return pastBlogs;
    }

    public void setPastBlogs(Set<Blog> pastBlogs) {
        this.pastBlogs = pastBlogs;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password=" + password
                + ", firstName=" + firstname + ", lastName=" + lastname
                + ", email=" + email + ", userRoles=" + role.toString() + "]";
    }
}
