package com.getserious.viral_blogger.web.controller;

import com.getserious.viral_blogger.models.Blog;
import com.getserious.viral_blogger.models.User;
import com.getserious.viral_blogger.exceptions.BloggerDataException;
import com.getserious.viral_blogger.services.BlogService;
import com.getserious.viral_blogger.services.UserService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@RestController
public class BlogRestController
{
    @Autowired BlogService blogService;
    @Autowired UserService userService;

    /**
     * Retrieves all users that are currently active in the database
     * @return A list of all the users
     */
    @RequestMapping(value="/api/users", method=RequestMethod.GET)
    public List<User> getAllUsers()
    {
        return userService.findAllUsers();
    }

    /**
     * Consumes a new user via the POST data sent and is then saved to the
     * database if possible. If there is an issue saving the BloggerDataException
     * caught will be forwarded to the front-end for user display
     * @param user the user to create
     * @return JSON formatted String containing status of success or error
     */
    @RequestMapping(value="/api/users", method=RequestMethod.POST)
    @SuppressWarnings("unchecked")
    public String createUser(@RequestBody User user)
    {
        System.out.println(user);
        JSONObject jsonObj = new JSONObject();
        try{
            userService.saveNewUser(user);
            jsonObj.put("success", "Account created! You can now login with your credentials!");
        }
        catch(BloggerDataException e) {
            jsonObj.put("error", e.toString());
        }
        return jsonObj.toJSONString();
    }

    /**
     * Returns a User json object based on the user name given. If there is no
     * user found this will return null.
     * @param username the username to use when searching the user table
     * @return The user found
     */
    @RequestMapping(value="/api/users/{username}", method=RequestMethod.GET)
    public User getUserByUsername(@PathVariable("username")String username)
    {
        return userService.findUserByUsername(username);
    }

    /**
     * Retrieves all blogs that are currently active in the database
     * @return A list of all active blogs
     */
    @RequestMapping(value="api/blogs", method=RequestMethod.GET)
    public List<Blog> getAllBlogs()
    {
        return blogService.findAllBlogs();
    }

    /**
     * Creates a new blog via the POST data sent and is then saved to the database
     * @param blog The new blog to create
     * @param principal The injected principal object that will be used to get the currently logged in user
     * @return The blog that was created
     */
    @RequestMapping(value="/api/blogs", method=RequestMethod.POST)
    public Blog createBlogPost(@RequestBody Blog blog, Principal principal)
    {
        // Use Principal to get authorized user that is logged in
        blog.setPostedBy(userService.findUserByUsername(principal.getName()));
        blog.setDatePosted(new Date());
        blogService.saveBlog(blog);

        return blog;
    }

}
