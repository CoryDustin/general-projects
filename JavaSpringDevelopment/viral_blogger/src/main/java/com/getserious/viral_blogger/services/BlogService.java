package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.models.Blog;

import java.util.List;
import java.util.Set;

public interface BlogService
{
    Blog findBlogById(int id);
    List<Blog> findAllBlogs();
    Set<Blog> findAllBlogsByUsername(String username);
    Blog saveBlog(Blog blog);
}
