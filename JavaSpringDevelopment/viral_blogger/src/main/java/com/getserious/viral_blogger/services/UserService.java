package com.getserious.viral_blogger.services;

import com.getserious.viral_blogger.exceptions.BloggerDataException;
import com.getserious.viral_blogger.models.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService extends UserDetailsService
{
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    User findUserById(int id);
    User findUserByUsername(String username);
    List<User> findAllUsers();
    User saveNewUser(User user) throws BloggerDataException;
    User saveUser(User user);
}
