var app = angular.module("bloggerApp");

app.service("requestService", function($http){

    this.ajaxPostRequest = function(url, data, successFn, failureFn)
    {
        console.log(data);
        $http.post(url, data)
             .then(successFn, failureFn);
    };
    
});