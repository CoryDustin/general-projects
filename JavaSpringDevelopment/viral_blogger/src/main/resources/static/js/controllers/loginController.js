"use strict";

var app = angular.module("bloggerApp");

app.controller("loginCtrl", function($scope, requestService){

    $scope.newUserInfo = {
        username: "",
        password: "",
        confirmPassword: "",
        firstname: "",
        lastname: "",
        email: ""
    };

    $scope.showSpinner = false;
    $scope.responseError = false;
    $scope.responseText = "";

    $scope.onSignUpButtonClick = function(){
        $("#sign-up-modal").openModal({complete: resetUserSignUpForm});
    };

    $scope.onSignUpSubmit = function()
    {
        $scope.showSpinner = true;
        requestService.ajaxPostRequest("/api/users", $scope.newUserInfo, onCreateUserSuccess, onCreateUserFailure);
    };

    var onCreateUserSuccess = function(response){
        // Checking if we have a success for creation of the user,
        // or if there was a soft error with a user already having an account
        if(response['data'].hasOwnProperty("success"))
        {
            resetUserSignUpForm();
            $scope.responseText = response['data']['success'];
        }
        else if(response['data'].hasOwnProperty("error"))
        {
            $scope.responseText = response['data']['error'];
            $scope.responseError = true;
            $scope.showSpinner = false;
        }
    };

    var onCreateUserFailure = function(response){
        $scope.responseText = response['data']['error'];
        $scope.responseError = true;
        $scope.showSpinner = false;
    };

    var resetUserSignUpForm = function(){
        $scope.signUpForm.$setPristine();
        $scope.signUpForm.$setUntouched();

        $scope.showSpinner = false;
        $scope.responseError = false;
        $scope.responseText = "";

        $scope.newUserInfo.username = "";
        $scope.newUserInfo.password = "";
        $scope.newUserInfo.confirmPassword = "";
        $scope.newUserInfo.firstname = "";
        $scope.newUserInfo.lastname = "";
        $scope.newUserInfo.email = "";
    };
});