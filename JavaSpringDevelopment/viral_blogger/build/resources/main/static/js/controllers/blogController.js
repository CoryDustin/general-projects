"use strict";

var app = angular.module("bloggerApp");

app.controller("blogCtrl", function($scope, requestService){

    $scope.blogPost = {
        title: "",
        content: "",
        commentCount: 0
    };

    $scope.blogPostList = [];
    var $newPostModal = $("#new-post-modal");

    $scope.onAddNewBlogClick = function(){
        $newPostModal.openModal({complete: resetBlogPostData});
    };

    $scope.onNewPostSubmit = function(){
        requestService.ajaxPostRequest("/api/blogs", $scope.blogPost, onCreateBlogSuccess, onCreateBlogFailure);
        // Use service to save new post via ajax rest call
    };

    var onCreateBlogSuccess = function(response)
    {
        $newPostModal.closeModal();
    };

    var onCreateBlogFailure = function(response)
    {

    };

    var resetBlogPostData = function()
    {
        $scope.blogPost.title = "";
        $scope.blogPost.content = "";
        $scope.commentCount = 0;
    };
});