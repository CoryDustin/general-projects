'use strict';

require('./vendor/materialize.js');
var angular = require('angular');

angular.module('blogApp', []);

require('./controllers/login-controller');

// We have to manually bootstrap the application for some reason???!?!?!?
angular.bootstrap(document, ['blogApp']);