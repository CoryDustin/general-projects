var webpack = require('webpack');

module.exports = {
    context: __dirname + '/public/javascripts',
    entry: {
        app: './app.js',
        vendor: ['jquery', 'hammerjs', 'angular']
    },
    output: {
        path: __dirname + '/public/javascripts/bundled',
        filename: 'blog.bundle.js'
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js')
    ]
};