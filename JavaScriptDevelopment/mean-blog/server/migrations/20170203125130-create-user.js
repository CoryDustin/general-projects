'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Users', {
      id: {
         allowNull: false,
         autoIncrement: true,
         primaryKey: true,
         type: Sequelize.INTEGER
      },
      firstname: {
         type: Sequelize.STRING,
         allowNull: false
      },
      lastname: {
         type: Sequelize.STRING,
         allowNull: false
      },
      username: {
         type: Sequelize.STRING,
         allowNull: false,
         unique: true
      },
      password: {
         type: Sequelize.STRING,
         allowNull: false
      },
      createdAt: {
         allowNull: false,
         type: Sequelize.DATE
      },
      updatedAt: {
         allowNull: false,
         type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Users');
  }
};