'use strict';
module.exports = {
   up: function(queryInterface, Sequelize) {
      return queryInterface.createTable('Posts', {
         id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
         },
         title: {
            type: Sequelize.STRING
         },
         body: {
            type: Sequelize.STRING
         },
         likeCount: {
            type: Sequelize.INTEGER,
            defaultValue: 0
         },
         createdAt: {
            allowNull: false,
            type: Sequelize.DATE
         },
         updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
         },
         userId: {
            type: Sequelize.INTEGER,
            onDelete: 'CASCADE',
            references: {
               model: 'Users',
               key: 'username',
               as: 'userId'
            }
         }
      });
   },
   down: function(queryInterface, Sequelize) {
      return queryInterface.dropTable('Posts');
   }
};