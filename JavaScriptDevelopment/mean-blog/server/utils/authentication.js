'use strict';

var User = require('../models').User;
var userController = require('../controllers/users');
var bcrypt = require('bcrypt');


module.exports = {

   signup: function(req, res){

      User
         .findOne({where: {username: req.body.username}})
         .then(function(user){
            console.log(user);
            if(user)
               res.status(401).json({error: 'That username is already in use, please try again'});
            else
               userController.create(req, res); // Create the user in the database
         })
         .catch(function(err){
            res.status(500).json({error: err});
         })

   },

   login: function(req, res){

      // Set up to be used with API request or POST login style
      var user = null;
      User.findOne({
         where: {
            username: req.body.username
         }
      })
      .then(function(userFound){
         if(userFound)
            user = userFound;
         else
            return res.status(401).json({error: 'Username or password was incorrect'});

         bcrypt.compare(req.body.password, user.password, function(err, hashresult){

            if(err)
               return res.status(401).json({error: 'An error occurred during login, please try again'});

            if(hashresult && !req.session.username){
               req.session.username = user.username;
               res.status(200).json({message: 'Login success'});
            }else if(req.session.username)
               res.status(401).json({error: 'An error occurred during login'});

         });
      })
      .catch(function(err){
         res.status(500).json({error: err});
      });
   },

   logout: function(req, res)
   {
      if(req.session.username) {
         console.log(req.session);
         var username = req.session.username;
         req.session.destroy();
         return res.status(200).json({message: username + " has been logged out."});
      }
      res.status(401).json({error: 'No session established to log out'});
   }
};