'use strict';

module.exports = function(sequelize, DataTypes)
{
   var User = sequelize.define('User', {
      firstname: {
         type: DataTypes.STRING,
         allowNull: false
      },
      lastname: {
         type: DataTypes.STRING,
         allowNull: false
      },
      username: {
         type: DataTypes.STRING,
         allowNull: false,
         unique: true
      },
      password: {
         type: DataTypes.STRING,
         allowNull: false
      }
   }, {
      classMethods: {
         associate: function(models) {

         }
      }
   });

   return User;
};