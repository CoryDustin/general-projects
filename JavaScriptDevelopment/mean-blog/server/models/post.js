'use strict';

module.exports = function(sequelize, DataTypes) {

   var Post = sequelize.define('Post', {
      title: {
         type: DataTypes.STRING,
         allowNull: false
      },
      body: {
         type: DataTypes.STRING,
         allowNull: false
      },
      likeCount: {
         type: DataTypes.INTEGER,
         defaultValue: 0
      }
   }, {
      classMethods: {
         associate: function(models) {
            // Add relationship that many posts belong to ONE User
            Post.belongsTo(models.User, {
               foreignKey: 'userId',
               onDelete: 'CASCADE'
            });
         }
      }
   });

   return Post;
};
