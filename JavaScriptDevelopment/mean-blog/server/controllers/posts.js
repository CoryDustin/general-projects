'use strict';

var Post = require('../models').Post;

module.exports = {

   create: function(req, res){
      return Post.create({
         title: req.body.title,
         body: req.body.body,
         userId: req.params.username
      })
      .then(function(post){
         res.status(201).json(post);
      })
      .catch(function(error){
         res.status(400).json(error.errors);
      });
   }

};
