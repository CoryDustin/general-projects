'use strict';

var User = require('../models').User;
var bcrypt = require('bcrypt');

module.exports = {

   create: function(req, res){

      bcrypt.hash(req.body.password, 10, function(err, hash){

         if(err) {
            return res.status(400).json({error: err});
         }

         return User.create({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            username: req.body.username,
            password: hash
         })
         .then(function(user){
            res.status(201).json(user);
         })
         .catch(function(err){
            res.status(500).json({error: err});
         });
      });

   },

   list: function(req, res){
      return User
         .all()
         .then(function(user){
            res.status(200).json(user);
         })
         .catch(function(error){
            res.status(500).json({error: err})
         });
   },

   findByUsername: function(req, res){

      // Set up to be used with API request or POST login style
      return User.findOne({
         where: {
            username: req.params.username
         }
      })
      .then(function(user){
         if(user)
            res.status(200).json(user);
         else
            res.status(400).json({error: 'No such user with that username'});
      })
      .catch(function(err){
         res.status(500).json({error: err});
      });
   }

};