'use strict';

var express = require('express');
var router = express.Router();

var users = require('../server/controllers').users;

router.get("/", function(req, res, next){
   res.status(200).json({
      message: "Welcome to the blogger api!"
   });
});

router.get('/users', users.list);
router.get('/users/:username', users.findByUsername);
router.post('/users', users.create);

module.exports = router;
