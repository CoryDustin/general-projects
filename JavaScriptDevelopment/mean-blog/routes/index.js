'use strict';

var express = require('express');
var router = express.Router();
var authentication = require('../server/utils/authentication');

/* GET home page. */
router.get('/', function(req, res, next) {
   res.render('index');
});

router.post('/login', authentication.login);
router.get('/logout', authentication.logout);
router.post('/signup', authentication.signup);

module.exports = router;
