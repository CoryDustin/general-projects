const config = {};

config.redisStore = {
   host: '127.0.0.1',
   port: 6380,
   prefex: 'session',
   secret: 'SECRET#+@'
};

module.exports = config;