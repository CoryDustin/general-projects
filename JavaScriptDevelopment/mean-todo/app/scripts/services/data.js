'use strict';

var angular = require('angular');

angular.module('todoListApp')
.service('dataService', function($http, $q) {
  this.getTodos = function(cb) {
    $http.get('/api/todos').then(cb);
  };

  this.deleteTodo = function(todo) {
    console.log("I deleted the " + todo.name + " todo!");
  };

  this.saveTodos = function(todos) {
    var requestList = [];
    todos.forEach(function(todo){
      var request;
      if(!todo._id){
        request = $http.post('/api/todos', todo);
      }
      else{
        request = $http.put('/api/todos/' + todo._id, todo).then(function(result){
          todo = result.data.todo;
        });
      }
      requestList.push(request);
    });
    return $q.all(requestList).then(function(results){ // Use this to send off multiple post requests to get saved
      console.log('I saved ' + todos.length + 'todos!');
    });
  };

});
