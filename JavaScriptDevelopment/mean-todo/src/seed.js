'use strict';

var Todo = require('./models/todo.js');

var todos = [
  'feed the dog',
  'walk the kids',
  'water the trees'
]

// Initializes data if there is none in the database; specified from the todos array
todos.forEach(function(todo, index){
  Todo.find({'name': todo}, function(err, todos){
    if(!err && !todos.length){
      Todo.create({completed: false, name: todo});
    }
  });
})
