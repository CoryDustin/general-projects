'use strict';

var express = require('express');
var Todo = require('../models/todo');
var jsonParser = require('body-parser').json();
// var todos = require('../../mock/todos.json');

var router = express.Router();

router.get('/todos', function(request, response){
  Todo.find({}, function(err, todos){
    if(err){
      return response.status(500).json({err: err.message});
    }
    response.json({todos: todos});
  }); // Returns all Todos in todo collection
});

// POST to add a new todo to the mongo database
router.post('/todos', jsonParser, function(request, response){

  var todo = request.body;
  Todo.create(todo, function(err, todo){
    if(err){
      return response.status(500).json({err: err.message});
    }
    response.json({'todo': todo, message: 'Todo Created'});
  });
});

// PUT to update the requested todo objects
router.put('/todos/:id', jsonParser, function(request, response){
  var id = request.params.id;
  var todo = request.body;
  if(todo && todo._id !== id){
    return response.status(500).json({err: "Todo IDs don't match!"});
  }
  // Find the todo with the specified ID and update it;
  // todo -> Json object with updated data to save
  Todo.findByIdAndUpdate(id, todo, {new: true}, function(err, todo){
    if(err){
      return response.status(500).json({err: err.message});
    }
    response.json({'todo': todo, message: 'Todo Updated!'});
  });
});

module.exports = router;
