'use strict';

var mongoose = require('mongoose');

// Creating a new todo schema for the model
var todoSchema = new mongoose.Schema({
  name: String,
  completed: Boolean
});

// register this to mongoose as the todo model
var model = mongoose.model('Todo', todoSchema);

module.exports = model;
