'use strict';

debugger;

var express = require('express');
var router = require('./api');

var app = express();

require('./database'); // Require database.js so we can run the database connect code
require('./seed'); // Require seed.js so we can populate the database with fake data

// Setting up routes
app.use('/', '../public/index.html');
app.use('/api', router);

app.listen(3000, function(){
  console.log("The server is running!!!");
});
