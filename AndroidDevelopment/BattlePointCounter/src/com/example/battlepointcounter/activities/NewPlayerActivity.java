package com.example.battlepointcounter.activities;

import java.util.HashMap;

import com.example.battelpointcounter.helper.PlayerDatabase;
import com.example.battelpointcounter.helper.Toaster;
import com.example.battlepointcounter.R;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.os.Build;

public class NewPlayerActivity extends Activity {

	private EditText playerNameEditText;
	private EditText playerBaseLevelEditText;
	private EditText playerArmorLevelEditText;
	private PlayerDatabase database;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_player);

		playerNameEditText	 	 = (EditText)findViewById(R.id.playerNameEditText);
		playerBaseLevelEditText  = (EditText)findViewById(R.id.playerBaseLevelEditText);
		playerArmorLevelEditText = (EditText)findViewById(R.id.playerArmorLevelEditText);
		database = new PlayerDatabase(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_player, menu);
		return true;
	}
	
	// Public Methods
	public void onBackButtonClick(View view)
	{
		Intent mainActivityIntent = new Intent(this, MainActivity.class);
		
		startActivity(mainActivityIntent);
	}
	
	public void onAddNewPlayerButtonClick(View view)
	{
		// If we have no data in any of the edit text fields prompt 
		// to complete the data needed to create a new player
		if(playerNameEditText.getText().toString().equals("") 	   || 
		   playerBaseLevelEditText.getText().toString().equals("") ||
		   playerArmorLevelEditText.getText().toString().equals(""))
		{
			
			Log.d("NEW PLAYER", "ADD BUTTON");
			Toaster toast = new Toaster(getApplicationContext());
			toast.make("Please Enter all required information");
		}
		else // We have all info so add player to SQLiteDB
		{
			HashMap<String, String> playerMap = new HashMap<String, String>();
			
			int playerOverallLevel = Integer.parseInt(playerBaseLevelEditText.getText().toString()) + 
									 Integer.parseInt(playerArmorLevelEditText.getText().toString());
			
			playerMap.put("playerName", playerNameEditText.getText().toString());
			playerMap.put("playerBaseLevel", playerBaseLevelEditText.getText().toString());
			playerMap.put("playerArmorLevel", playerArmorLevelEditText.getText().toString());
			playerMap.put("playerOverallLevel", playerOverallLevel + "");
			
			database.insertPlayer(playerMap);
			
			Toaster toast = new Toaster(getApplicationContext());
			toast.make("Player " + playerNameEditText.getText().toString() + " was added!");
			
			Intent mainActivityIntent = new Intent(this, MainActivity.class);
			
			startActivity(mainActivityIntent);
		}
	}

}
