package com.example.battlepointcounter.activities;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.battelpointcounter.helper.PlayerDatabase;
import com.example.battlepointcounter.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity 
{
	private LinearLayout scrollViewLinearLayout;
	private PlayerDatabase database;
	private ArrayList<HashMap<String, String>> currentPlayersList;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            
        }
        database = new PlayerDatabase(this);
        
        scrollViewLinearLayout = (LinearLayout)findViewById(R.id.playerBoardScrollLinearLayout);

        currentPlayersList = database.getAllPlayers();
        
        for(HashMap<String, String> playerMap : currentPlayersList)
        {
        	addNewPlayerViewToScrollList(playerMap);
        }
    }
    
    // Public Methods
    
    /**
     * Custom callback method for add player button click
     * @param view
     */
    public void onAddPlayerButtonClick(View view)
    {
    	Intent newPlayerActivityIntent = new Intent(this, NewPlayerActivity.class);
    	
    	startActivity(newPlayerActivityIntent);
    }
    
    
    // Private Methods
    private void addNewPlayerViewToScrollList(HashMap<String, String> playerMapData)
    {
    	LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
    	View newPlayerItemView = inflater.inflate(R.layout.player_scroll_item, null);
    	
    	TextView playerDataTextView 	= (TextView)newPlayerItemView.findViewById(R.id.playerDataTextView);
    	TextView playerIdTextView 		= (TextView)newPlayerItemView.findViewById(R.id.playerIdTextView);
    	ImageButton deletePlayerButton 	= (ImageButton)newPlayerItemView.findViewById(R.id.deletePlayerButton);
    	
    	playerDataTextView.setText(playerMapData.get("playerName") + ": " + playerMapData.get("playerOverallLevel"));
    	playerIdTextView.setText(playerMapData.get("playerId"));
    	
    	newPlayerItemView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) 
			{
				Intent playerStatsActivityIntent = new Intent(getApplicationContext(), PlayerStats.class);
				TextView playerIdTextView = (TextView)v.findViewById(R.id.playerIdTextView);
				
				playerStatsActivityIntent.putExtra("playerId", playerIdTextView.getText().toString());
				
				startActivity(playerStatsActivityIntent);
				
			}
    		
    	});
    	
    	deletePlayerButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				PlayerDatabase database = new PlayerDatabase(getApplicationContext());
				
				// Get player id text view from parent view
				TextView playerIdTextView = (TextView)((View)v.getParent()).findViewById(R.id.playerIdTextView);
				
				database.deletePlayer(playerIdTextView.getText().toString());
				
				// Remove all views
				scrollViewLinearLayout.removeAllViews();
				
				// Added remaining views that were not deleted back to scroll view
				for(HashMap<String, String> playerMap : database.getAllPlayers())
				{
					addNewPlayerViewToScrollList(playerMap);
				}
			}
		});
    	
    	scrollViewLinearLayout.addView(newPlayerItemView);
    }
    
    



}
