package com.example.battlepointcounter.activities;

import java.util.HashMap;

import com.example.battelpointcounter.helper.PlayerDatabase;
import com.example.battlepointcounter.R;
import com.example.battlepointcounter.R.id;
import com.example.battlepointcounter.R.layout;
import com.example.battlepointcounter.R.menu;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Build;

public class PlayerStats extends Activity 
{
	private TextView playerNameTextView;
	private TextView playerOverallLevelTextView;
	private TextView playerLevelTextView;
	private TextView playerArmorTextView;
	
	private String playerId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player_stats);

		initPlayerStatsActivity();
	}


	
	private void initPlayerStatsActivity()
	{
		playerNameTextView 		   = (TextView)findViewById(R.id.playerStatsNameTextView);
		playerOverallLevelTextView = (TextView)findViewById(R.id.playerStatsOverallLevelTextView);
		playerLevelTextView        = (TextView)findViewById(R.id.playerStatsLevelTextView);
		playerArmorTextView   	   = (TextView)findViewById(R.id.playerStatsArmorTextView);
		
		Intent intent = getIntent();
		playerId = intent.getStringExtra("playerId");
		
		PlayerDatabase database = new PlayerDatabase(this);
		HashMap<String, String> playerMap = database.getPlayerById(playerId);
		
		playerNameTextView.setText(playerMap.get("playerName"));
		playerOverallLevelTextView.setText(playerMap.get("playerOverallLevel"));
		playerLevelTextView.setText(playerMap.get("playerBaseLevel"));
		playerArmorTextView.setText(playerMap.get("playerArmorLevel"));
	}
	
	public void onBackButtonClick(View view)
	{
		PlayerDatabase database = new PlayerDatabase(this);
		HashMap<String, String> playerMap = new HashMap<String, String>();
		
		playerMap.put("playerId", playerId);
		playerMap.put("playerName", playerNameTextView.getText().toString());
		playerMap.put("playerBaseLevel", playerLevelTextView.getText().toString());
		playerMap.put("playerArmorLevel", playerArmorTextView.getText().toString());
		playerMap.put("playerOverallLevel", playerOverallLevelTextView.getText().toString());
		
		database.updatePlayer(playerMap);

		Intent mainActivityIntent = new Intent(this, MainActivity.class);
		
		startActivity(mainActivityIntent);
	}
	
	public void onArmorLevelUpButtonClick(View view)
	{
		int armorLevel = Integer.parseInt(playerArmorTextView.getText().toString());
		armorLevel++;
		armorLevel = clamp(armorLevel, 0, 100);
		
		playerArmorTextView.setText(armorLevel + "");
		playerOverallLevelTextView.setText(calculateOverallLevel() + "");
	}
	
	public void onArmorLevelDownButtonClick(View view)
	{
		int armorLevel = Integer.parseInt(playerArmorTextView.getText().toString());
		armorLevel--;
		armorLevel = clamp(armorLevel, 0, 100);
		
		playerArmorTextView.setText(armorLevel + "");
		playerOverallLevelTextView.setText(calculateOverallLevel() + "");
	}
	
	public void onPlayerBaseLevelUpButtonClick(View view)
	{
		int playerLevel = Integer.parseInt(playerLevelTextView.getText().toString());
		playerLevel++;
		playerLevel = clamp(playerLevel, 0, 100);
		
		playerLevelTextView.setText(playerLevel + "");
		playerOverallLevelTextView.setText(calculateOverallLevel() + "");
	}
	
	public void onPlayerBaseLevelDownButtonClick(View view)
	{
		int playerLevel = Integer.parseInt(playerLevelTextView.getText().toString());
		playerLevel--;
		playerLevel = clamp(playerLevel, 0, 100);
		
		playerLevelTextView.setText(playerLevel + "");
		playerOverallLevelTextView.setText(calculateOverallLevel() + "");
	}
	
	private int calculateOverallLevel()
	{
		int playerLevel = Integer.parseInt(playerLevelTextView.getText().toString());
		int armorLevel  = Integer.parseInt(playerArmorTextView.getText().toString());
		
		return playerLevel + armorLevel;
	}
	
	private int clamp(int value, int min, int max)
	{
		return Math.max(min, Math.min(max, value));
	}

}
