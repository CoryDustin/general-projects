package com.example.battlepointcounter.model;

public class PlayerData 
{
	private String name;
	private int baseLevel;
	private int armorLevel;
	private int overallLevel;
	
	public PlayerData(String name, int baseLevel, int armorLevel)
	{
		this.name = name;
		this.baseLevel = baseLevel;
		this.armorLevel = armorLevel;
		this.overallLevel = baseLevel + armorLevel;
	}
	
	public PlayerData(String name)
	{
		this.name = name;
		this.baseLevel = 1;
		this.armorLevel = 0;
		this.overallLevel = baseLevel + armorLevel;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBaseLevel() {
		return baseLevel;
	}
	public void setBaseLevel(int baseLevel) {
		this.baseLevel = baseLevel;
	}
	public int getArmorLevel() {
		return armorLevel;
	}
	public void setArmorLevel(int armorLevel) {
		this.armorLevel = armorLevel;
	}
	public int getOverallLevel() {
		return overallLevel;
	}
	public void setOverallLevel(int overallLevel) {
		this.overallLevel = overallLevel;
	}

	
	
}
