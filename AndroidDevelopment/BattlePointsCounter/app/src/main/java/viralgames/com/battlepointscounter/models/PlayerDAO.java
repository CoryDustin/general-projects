package viralgames.com.battlepointscounter.models;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerDAO extends SQLiteOpenHelper
{

    public PlayerDAO(Context context)
    {
        super(context, "playerdatabase.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String query = "CREATE TABLE players(id INTEGERY PRIMARY KEY, name TEXT, baseLevel INTEGER," +
                                            "armorLevel INTEGER, overallLevel INTEGER)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        String query = "DROP TABLE IF EXISTS players";
        db.execSQL(query);
        this.onCreate(db);
    }

    public void insertPlayer(Player player)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = getPlayerContentValues(player);

        database.insert("players", null, values);
        database.close();
    }

    public void updatePlayer(Player player, int playerId)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = getPlayerContentValues(player);

        database.update("players", values, "id = ?", new String[]{String.valueOf(playerId)});
        database.close();
    }

    public void deletePlayer(int playerId)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        String query = "DELETE FROM players WHERE id='" + playerId + "'";
        database.execSQL(query);
    }

    public ArrayList<Player> getAllPlayers()
    {
        ArrayList<Player> playerArrayList = new ArrayList<Player>();
        String query = "SELECT * from players ORDER BY name";

        // Cursor, provides Read/Write access to data that is
        // returned from rawQuery method.
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        if(cursor.moveToFirst())
        {
            do
            {
                Player player = new Player(cursor.getInt(0), cursor.getString(1),
                                           cursor.getInt(2), cursor.getInt(3), cursor.getInt(4));
                playerArrayList.add(player);

            }while(cursor.moveToNext());
        }

        return playerArrayList;
    }

    public Player getPlayer(int playerId)
    {
        String query = "SELECT * FROM players WHERE id='" + playerId + "'";

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        Player player = null;

        if(cursor.moveToFirst())
        {
            player = new Player(cursor.getInt(0), cursor.getString(1),
                                cursor.getInt(2), cursor.getInt(3), cursor.getInt(4));
        }
        return player;
    }

    private ContentValues getPlayerContentValues(Player player)
    {
        ContentValues values = new ContentValues();

        values.put("name", player.getName());
        values.put("baseLevel", player.getBaseLevel());
        values.put("armorLevel", player.getArmorLevel());
        values.put("overallLevel", player.getOverallLevel());
        return values;
    }
}
