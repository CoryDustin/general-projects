package viralgames.com.battlepointscounter.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

import butterknife.ButterKnife;
import viralgames.com.battlepointscounter.models.Player;
import viralgames.com.battlepointscounter.models.PlayerDAO;
import viralgames.com.battlepointscounter.R;

public class MainActivity extends AppCompatActivity {

    @Nullable @Bind(R.id.playerBoardScrollLinearLayout) protected LinearLayout scrollViewLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        PlayerDAO playerDAO = new PlayerDAO(this);
        addAllPlayersToScrollView(playerDAO.getAllPlayers());

    }

    @Nullable
    @OnClick(R.id.addNewPlayerButton)
    public void onAddPlayerButtonClick(View view)
    {
        Intent newPlayerIntent = new Intent(this, NewPlayerActivity.class);
        startActivity(newPlayerIntent);
    }

    private void addAllPlayersToScrollView(ArrayList<Player> playerList)
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newPlayerItemView = inflater.inflate(R.layout.player_scroll_item, null);

        TextView playerDataTextView = (TextView)newPlayerItemView.findViewById(R.id.playerDataTextView);
        ImageButton deletePlayerButton = (ImageButton)newPlayerItemView.findViewById(R.id.deletePlayerButton);

        for(final Player player : playerList)
        {
            playerDataTextView.setText(player.getName() + ": " + player.getOverallLevel());
            playerDataTextView.setTag(R.id.playerId, player.getId());

            newPlayerItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent playerStatsIntent = new Intent(getApplicationContext(), PlayerStatsActivity.class);
                    playerStatsIntent.putExtra("parcelPlayer", player);
                    startActivity(playerStatsIntent);
                }
            });

            deletePlayerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    PlayerDAO playerDAO = new PlayerDAO(getApplicationContext());
                    int playerId = (Integer)((View)v.getParent()).getTag(R.id.playerId);

                    playerDAO.deletePlayer(playerId);

                    scrollViewLinearLayout.removeView((View)v.getParent());
                }
            });

            scrollViewLinearLayout.addView(newPlayerItemView);
        }


    }


}
