package viralgames.com.battlepointscounter.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.Bind;
import viralgames.com.battlepointscounter.R;

public class NewPlayerActivity extends AppCompatActivity {

    @Bind(R.id.playerNameEditText) EditText playerNameEditText;
    @Bind(R.id.playerBaseLevelEditText) EditText playerBaseEditText;
    @Bind(R.id.playerArmorLevelEditText) EditText playerArmorEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);



    }
}
