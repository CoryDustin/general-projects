package viralgames.com.battlepointscounter.models;


import android.os.Parcel;
import android.os.Parcelable;

public class Player implements Parcelable
{
    private String name;
    private int baseLevel;
    private int armorLevel;
    private int overallLevel;
    private int id;

    public Player()
    {
        this.name = "";
        this.baseLevel = 1;
        this.armorLevel = 0;
        this.overallLevel = 1;
        this.id = -1;
    }

    public Player(String name)
    {
        this.name = name;
        this.baseLevel = 1;
        this.armorLevel = 0;
        this.overallLevel = 1;
        this.id = -1;
    }

    public Player(String name, int baseLevel, int armorLevel)
    {
        this.name = name;
        this.baseLevel = baseLevel;
        this.armorLevel = armorLevel;
        this.overallLevel = baseLevel + armorLevel;
        this.id = -1;
    }

    public Player(String name, int baseLevel, int armorLevel, int overallLevel)
    {
        this.name = name;
        this.baseLevel = baseLevel;
        this.armorLevel = armorLevel;
        this.overallLevel = overallLevel;
        this.id = -1;
    }

    public Player(int id, String name, int baseLevel, int armorLevel, int overallLevel)
    {
        this.id = id;
        this.name = name;
        this.baseLevel = baseLevel;
        this.armorLevel = armorLevel;
        this.overallLevel = overallLevel;
    }

    /**
     * Constructor needed for the Parcelable CREATOR attribute
     * @param parcel
     */
    public Player(Parcel parcel)
    {
        this.name = parcel.readString();
        this.baseLevel = parcel.readInt();
        this.armorLevel = parcel.readInt();
        this.overallLevel = parcel.readInt();
        this.id = parcel.readInt();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseLevel() {
        return baseLevel;
    }

    public void setBaseLevel(int baseLevel) {
        this.baseLevel = baseLevel;
    }

    public int getArmorLevel() {
        return armorLevel;
    }

    public void setArmorLevel(int armorLevel) {
        this.armorLevel = armorLevel;
    }

    public int getOverallLevel() {
        return overallLevel;
    }

    public void setOverallLevel(int overallLevel) {
        this.overallLevel = overallLevel;
    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.baseLevel);
        dest.writeInt(this.armorLevel);
        dest.writeInt(this.overallLevel);
        dest.writeInt(this.id);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
