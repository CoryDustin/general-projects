package example.com.stormy.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import example.com.stormy.R;
import example.com.stormy.weather.Day;

/**
 * Created by Cory on 4/29/2015.
 */
public class DayAdapter extends BaseAdapter
{
    private Context context;
    private Day[] days;

    public DayAdapter(Context context, Day[] days)
    {
        this.context = context;
        this.days = days;
    }

    @Override
    public int getCount()
    {
        return days.length;
    }

    @Override
    public Object getItem(int position)
    {
        return days[position];
    }

    @Override
    public long getItemId(int position)
    {
        return 0; // Not using but can be used to tag items for easy reference
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        // If we need to create the view to be loaded then create a new one
        if(convertView == null)
        {
            // Set up new view for use in list
            convertView = LayoutInflater.from(context).inflate(R.layout.daily_list_item, null);

            holder = new ViewHolder();

            holder.iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
            holder.temperatureLabel = (TextView)convertView.findViewById(R.id.temperatureLabel);
            holder.dayLabel = (TextView)convertView.findViewById(R.id.dayNameLabel);

            convertView.setTag(holder);
        }
        // else we reuse a previous view created and set the new data
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        Day day = days[position];

        holder.iconImageView.setImageResource(day.getIconId());
        holder.temperatureLabel.setText(day.getTemperatureMax() + "");

        if(position == 0)
            holder.dayLabel.setText("Today");
        else
            holder.dayLabel.setText(day.getDayOfTheWeek());

        return convertView;
    }

    private static class ViewHolder
    {
        public ImageView iconImageView;
        public TextView temperatureLabel;
        public TextView dayLabel;
    }
}
