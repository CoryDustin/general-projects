package example.com.stormy.weather;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Cory on 4/25/2015.
 * Implements Parcelable to ensure this class can
 * be passed as extra content from one activity to another
 */
public class Day implements Parcelable
{
    private long time;
    private String summary;
    private double temperatureMax;
    private String icon;
    private String timeZone;

    public Day(){  }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public int getTemperatureMax()
    {
        return (int)Math.round(temperatureMax);
    }

    public void setTemperatureMax(double temperatureMax)
    {
        this.temperatureMax = temperatureMax;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getTimeZone()
    {
        return timeZone;
    }

    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public int getIconId()
    {
        return Forecast.getIconId(icon);
    }

    public String getDayOfTheWeek()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));

        // set date time from time long and multiply by 1000 to convert to milliseconds
        Date dateTime = new Date(time * 1000);

        return formatter.format(dateTime);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(time);
        dest.writeString(summary);
        dest.writeDouble(temperatureMax);
        dest.writeString(icon);
        dest.writeString(timeZone);
    }

    // Creating private constructor to set all class data
    // if this class has been parsed from one activity to another
    private Day(Parcel in)
    {
        time = in.readLong();
        summary = in.readString();
        temperatureMax = in.readDouble();
        icon = in.readString();
        timeZone = in.readString();
    }

    // Must have member field called CREATOR of type Create<Day>
    // to ensure Parcelable works properly
    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
