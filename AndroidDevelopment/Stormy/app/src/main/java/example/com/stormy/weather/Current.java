package example.com.stormy.weather;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import example.com.stormy.R;

/**
 * Created by Cory on 3/2/2015.
 */
public class Current
{
    private String icon;
    private long time;
    private double temperature;
    private double humidity;
    private double precipChance;
    private String summary;
    private String timeZone;
    private String location;

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public int getIconId()
    {
        return Forecast.getIconId(icon);
    }

    public long getTime()
    {
        return time;
    }

    public String getFormattedTime()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));

        // Multiply by 1000 since date class needs milliseconds and not seconds
        // which is how the time variable is stored
        Date dateTime = new Date(time * 1000);
        String timeString = formatter.format(dateTime);

        return timeString;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    public int getTemperature()
    {
        return (int)Math.round(temperature);
    }

    public void setTemperature(double temperature)
    {
        this.temperature = temperature;
    }

    public double getHumidity()
    {
        return humidity;
    }

    public void setHumidity(double humidity)
    {
        this.humidity = humidity;
    }

    public int getPrecipChance()
    {
        double precipPercentage = precipChance * 100;
        return (int)Math.round(precipChance);
    }

    public void setPrecipChance(double precipChance)
    {
        this.precipChance = precipChance;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getTimeZone()
    {
        return timeZone;
    }

    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

}
