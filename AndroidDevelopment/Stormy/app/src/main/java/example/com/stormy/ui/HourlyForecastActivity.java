package example.com.stormy.ui;

import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import example.com.stormy.R;
import example.com.stormy.adapters.HourAdapter;
import example.com.stormy.weather.Hour;

public class HourlyForecastActivity extends ActionBarActivity {

    private Hour[] hours;

    @InjectView(R.id.recyclerView) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hourly_forecast);

        ButterKnife.inject(this);

        // Get the hours parceable data that was passed from the intent that opened this activity
        Parcelable[] parcelablesHours = getIntent().getParcelableArrayExtra(MainActivity.HOURLY_FORECAST);
        hours = Arrays.copyOf(parcelablesHours, parcelablesHours.length, Hour[].class);

        // Set the hour adapter
        HourAdapter adapter = new HourAdapter(this, hours);
        recyclerView.setAdapter(adapter);

        // Set up the layout manager that manages the views in the recycler list view
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);
    }

}
