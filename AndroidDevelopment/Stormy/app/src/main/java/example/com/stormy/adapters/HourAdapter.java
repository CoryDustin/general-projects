package example.com.stormy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import example.com.stormy.R;
import example.com.stormy.weather.Hour;

/**
 * Created by Cory on 5/3/2015.
 */
public class HourAdapter extends RecyclerView.Adapter<HourAdapter.HourViewHolder>
{

    private Hour[] hours;
    private Context context;

    public HourAdapter(Context context, Hour[] hours)
    {
        this.context = context;
        this.hours = hours;
    }

    @Override
    public HourViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hourly_list_item, viewGroup, false);
        HourViewHolder viewHolder = new HourViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HourViewHolder hourViewHolder, int i)
    {
        hourViewHolder.bindHour(hours[i]);
    }

    @Override
    public int getItemCount()
    {
        return hours.length;
    }

    public class HourViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public RelativeLayout rootParentLayout;
        public TextView timeLabel;
        public TextView summaryLabel;
        public TextView temperatureLabel;
        public ImageView iconImageView;

        public HourViewHolder(View itemView)
        {
            super(itemView);

            rootParentLayout = (RelativeLayout)itemView.findViewById(R.id.hourlyRootRelativeLayout);

            timeLabel = (TextView)itemView.findViewById(R.id.timeLabel);
            summaryLabel = (TextView)itemView.findViewById(R.id.summaryLabel);
            temperatureLabel = (TextView)itemView.findViewById(R.id.temperatureLabel);
            iconImageView = (ImageView)itemView.findViewById(R.id.iconImageView);

            itemView.setOnClickListener(this);
        }

        public void bindHour(Hour hour)
        {
            timeLabel.setText(hour.getHour());
            summaryLabel.setText(hour.getSummary());
            temperatureLabel.setText(hour.getTemperature() + "");
            iconImageView.setImageResource(hour.getIconId());

            // Animate the root parent hourly layout to come in from left of screen
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            rootParentLayout.startAnimation(animation);
        }

        @Override
        public void onClick(View v)
        {
            String time = timeLabel.getText().toString();
            String temperature = temperatureLabel.getText().toString();
            String summary = summaryLabel.getText().toString();
            String message = "At " + time + " it will be " + temperature + " and " + summary;

            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
