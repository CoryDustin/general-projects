package example.com.stormy.ui;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.util.StringBuilderPrinter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import example.com.stormy.R;
import example.com.stormy.adapters.DayAdapter;
import example.com.stormy.weather.Day;

public class DailyForecastActivity extends ActionBarActivity
{
    private Day[] days;
    @InjectView(android.R.id.list) ListView listView;
    @InjectView(android.R.id.empty) TextView emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_forecast);

        ButterKnife.inject(this);

        Parcelable[] parcelableDays = getIntent().getParcelableArrayExtra(MainActivity.DAILY_FORECAST);
        days = Arrays.copyOf(parcelableDays, parcelableDays.length, Day[].class);

        DayAdapter adapter = new DayAdapter(this, days);

        listView.setAdapter(adapter);
        listView.setEmptyView(emptyTextView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String dayOfTheWeek = days[position].getDayOfTheWeek();
                String conditions = days[position].getSummary();
                String highTemp = days[position].getTemperatureMax() + "";
                String message = "On " + dayOfTheWeek + " the high will be " + highTemp + " and it will be " + conditions;

                Toast.makeText(DailyForecastActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}


