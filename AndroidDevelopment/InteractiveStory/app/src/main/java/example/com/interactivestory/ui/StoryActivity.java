package example.com.interactivestory.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import example.com.interactivestory.model.Page;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import example.com.interactivestory.R;
import example.com.interactivestory.model.Story;


public class StoryActivity extends Activity
{

    private Story story = new Story();
    private ImageView imageView;
    private TextView textView;
    private Button choice1Button;
    private Button choice2Button;

    private String name;
    private Page currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        Intent intent = getIntent();
        name = intent.getStringExtra(getString(R.string.key_name));

        imageView = (ImageView)findViewById(R.id.storyImageView);
        textView = (TextView)findViewById(R.id.storyTextView);
        choice1Button = (Button)findViewById(R.id.choiceButton1);
        choice2Button = (Button)findViewById(R.id.choiceButton2);

        loadPage(0);
    }

    private void loadPage(int choiceIndex)
    {
        currentPage = story.getPage(choiceIndex);

        Drawable drawable = getResources().getDrawable(currentPage.getImageId());
        imageView.setImageDrawable(drawable);

        String pageText = currentPage.getText();
        // Adds the users name if the place holder appears in the page text
        pageText = String.format(pageText, name);
        textView.setText(pageText);

        // If we are on the final page then hide the choice buttons!
        if(currentPage.isFinal())
        {
            choice1Button.setVisibility(View.INVISIBLE);
            choice2Button.setText("PLAY AGAIN");
            choice2Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    // Will finish the current Activity and go back to the previous
                    // Activity that opened this Activity
                    finish();
                }
            });
        }
        else
        {
            choice1Button.setText(currentPage.getChoice1().getText());
            choice2Button.setText(currentPage.getChoice2().getText());
        }

        choice1Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int nextPage = currentPage.getChoice1().getNextPage();
                loadPage(nextPage);
            }
        });

        choice2Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int nextPage = currentPage.getChoice2().getNextPage();
                loadPage(nextPage);
            }
        });
    }
}
