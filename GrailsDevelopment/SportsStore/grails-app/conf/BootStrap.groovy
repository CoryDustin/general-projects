import sportsstore.Product

class BootStrap {

    def init = { servletContext ->
		new Product(name: "Kayak", description: "A boat for one person", category: "Watersports", price: 275).save()
		new Product(name: "Lifejacket", description: "Protective and fashionable", category: "Watersports", price: 48.95).save()
		new Product(name: "Soccer ball", description: "FIFA-approved size and weight", category: "Soccer", price: 19.5).save()
		new Product(name: "Corner Flags", description: "Give your playing field a professional touch", category: "Soccer", price: 34.95).save()
		new Product(name: "Stadium", description: "Flat-packed 35,000-seat stadium", category: "Soccer", price: 79500).save()
		new Product(name: "Thinking Cap", description: "Improve your brain effciency by 75%", category: "Chess", price: 16).save()
		new Product(name: "Unsteady Chair", description: "Secretly give your opponent a disadvantage", category: "Chess", price: 29.95).save()
		new Product(name: "Human Chess Board", description: "A fun game for the family", category: "Chess", price: 75).save()
		new Product(name: "Bling-Bling King", description: "Gold-plated, diamond-studded King", category: "Chess", price: 1200).save()
    }
    def destroy = {
    }
}
