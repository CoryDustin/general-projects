<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons"/>
<link type="text/css" rel="stylesheet" href="${resource(dir:'css', file:'materialize.css')}"/>
<g:javascript src="thirdparty/angular.js"/>

<!-- Let browser know website is optimized for mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1" />