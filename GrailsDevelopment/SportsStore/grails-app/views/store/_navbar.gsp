<!-- Navbar display -->
<nav>
	<div class="nav-wrapper blue-grey darken-3">
		<a href="#" class="brand-logo">SPORTS STORE</a>
		<a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
		
		<!-- The desktop nav menu 
		<ul class="right hide-on-med-and-down">
		</ul> -->
	</div>
</nav>