<!DOCTYPE html>
<html ng-app="todoApp">
	<head>
		<title>TODO List</title>
		<g:render template="header"/>
		<script>
			var todoApp = angular.module("todoApp", []);

			var model = {
				user: "Cory"
			};

			todoApp.run(function($http){
				$http.get("json/todo.json").success(function(data){
					model.items = data;	
				});
			});
			
			todoApp.filter("checkedItems", function(){
				return function(items, showComplete){
					var resultArr = [];
					angular.forEach(items, function(item){
						if(!item.done || showComplete)
							resultArr.push(item);
					});
					return resultArr;
				}
			});
			
			todoApp.controller("ToDoCtrl", function($scope){
				$scope.todo = model;

				$scope.incompleteCount = function(){
					var count = 0; 
					angular.forEach($scope.todo.items, function(item){
						if(!item.done)
							count++;
					});
					return count;
				};

				$scope.warningLevel = function(){
					return $scope.incompleteCount() < 3 ? "green-text" : "red-text";
				};

				$scope.addNewItem = function(todoText){
					return $scope.todo.items.push({action: todoText, done: false});
				}
			});
		</script>
	</head>
	
	<body ng-controller="ToDoCtrl">
		<div class="container">
			<h1 class="center">
				{{todo.user}}'s To Do List!
				<span ng-class="warningLevel()" ng-hide="incompleteCount() == 0">
					{{incompleteCount()}}
				</span>
			</h1>
			<div class="row">
				<div class="col s8">
					<div class="input-field">
						<input ng-model="todoText" type="text" placeholder="New todo item" id="todo-input" class="validate"/>
						<label for="todo-input">Todo Item</label>
					</div>
				</div>
				<div class="col s4">
					<button ng-click="addNewItem(todoText)" class="btn waves-effect waves-light-btn">Add</button>
				</div>
			</div>
			<table>
				<thead>
					<tr>
						<th data-field="description">Description</th>
						<th data-field="done">Done</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in todo.items | checkedItems:showComplete | orderBy:'action'">
						<td>{{item.action}}</td>
						<td>
							<input type="checkbox" ng-model="item.done" id="done-checkbox-{{$index}}"/>
							<label for="done-checkbox-{{$index}}"></label>
						</td>
					</tr>
				</tbody>
			</table>
			<!-- The show completed todos checkbox, will display all todos in todo items array if checked -->
			<input type="checkbox" ng-model="showComplete" id="showcomplete-checkbox"/>
			<label for="showcomplete-checkbox">Show Complete</label>
		</div>

		<g:render template="footer"/>
	</body>
	
</html>