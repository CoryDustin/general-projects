package sportsstore

class Product {

	String name
	String description
	String category
	Float price
	
    static constraints = {
    }
}
