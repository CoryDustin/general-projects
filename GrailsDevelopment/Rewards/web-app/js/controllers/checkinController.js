"use strict";

var checkinApp = angular.module("checkinApp", []);

checkinApp.controller("CheckinController", function($scope){
	
	$scope.phoneNumber = "";
	
	$scope.onKeypadClick = function(num){
		$scope.phoneNumber += num;
	};
	
	$scope.onDeleteClick = function(){
		$scope.phoneNumber = $scope.phoneNumber.substring(0, $scope.phoneNumber.length-1);
	}
});