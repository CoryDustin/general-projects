<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<img src="../images/banner.png"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5 col-sm-offset-1">
			<h4>${welcomeMessage}</h4>
		</div>
		<div class="col-sm-6">
			<g:textField ng-model="phoneNumber" name="phone" class="form-control" 
				placeholder="Enter your cell phone number to checkin" value="${customerInstance?.phone}"/>
			<div class="row">
				<h4></h4>
			</div>
			
			<div class="row">
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(1)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="1"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(2)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="2"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(3)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="3"/>
				</div>
			</div>
			<div class="row">
				<h4></h4>
			</div>
			
			<div class="row">
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(4)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="4"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(5)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="5"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(6)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="6"/>
				</div>
			</div>
			<div class="row">
				<h4></h4>
			</div>
			
			<div class="row">
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(7)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="7"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(8)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="8"/>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(9)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="9"/>
				</div>
			</div>
			<div class="row">
				<h4></h4>
			</div>
			
			<div class="row">
				<div class="col-sm-4">
					<g:link class="btn btn-danger btn-lg btn-block" action="checkin">Del</g:link>
					<%--<input ng-click="onDeleteClick()" class="btn btn-danger btn-lg btn-block" type="button" name="pad" value="Del"/>--%>
				</div>
				<div class="col-sm-4">
					<input ng-click="onKeypadClick(0)" class="btn btn-primary btn-lg btn-block" type="button" name="pad" value="0"/>
				</div>
				<div class="col-sm-4">
					<g:submitButton class="btn btn-success btn-lg btn-block" name="pad" value="Go"/>
					<%--<input ng-click="" class="btn btn-success btn-lg btn-block" type="button" name="pad" value="Go"/>--%>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End of container -->