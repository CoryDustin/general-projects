<!doctype html>
<html ng-app="checkinApp">

<head>
	<title>Kiosk</title>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
	<%--<script src="../js/respond.js"></script>--%>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
	<script src="../js/controllers/checkinController.js"></script>
</head>

<body ng-controller="CheckinController">
	<g:form url="[resource:customerInstance, action:'customerLookup']" >
			<g:render template="kioskForm"/>
	</g:form>
	
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   
			integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   
			crossorigin="anonymous"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>