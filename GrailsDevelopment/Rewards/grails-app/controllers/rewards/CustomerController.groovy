package rewards

class CustomerController {

	static scaffold = true
	def calculationsService
	
	def lookup(){
		// Find all users by last name that begins with 'B', Insensitive, remove I for sensitive
		// findAllByTotalPointsBetween(2, 4, [sort: "totalPoints"]) Find record with total points between 2 and 4 and sort by totalPoints
		// findAllByTotalPointsGreaterThan(3, [sort: "totalPoints", order: "desc"]) Total points greater than 3 sorted by totalPoints and show in descending order
		def customerInstance = Customer.findAllByLastNameIlike("B%")
		[customerInstanceList: customerInstance]
	}
	
	def checkin(){}
	
	def profile(){
		def customerInstance = Customer.findByPhone(params.id)
		[customerInstance: customerInstance]
	}
	
	def updateProfile(Customer customerInstance){
		customerInstance.save()
		render(view: "profile", model: [customerInstance: customerInstance])
	}
	
	def customerLookup(Customer lookupInstance){
		def (customerInstance, welcomeMessage) = calculationsService.processCheckin(lookupInstance)
		render(view: "checkin", model: [customerInstance: customerInstance, welcomeMessage: welcomeMessage])
	}
	
	def index() { 
		params.max = 10
		[customerInstanceList: Customer.list(params), customerInstanceCount: Customer.count()]	
	}
	
	def create(){
		[customerInstance: new Customer()]
	}
	
	def save(Customer customerInstance){
		customerInstance.save()
		redirect(action: "show", id: customerInstance.id)
	}
	
	def show(Long id){
		def customerInstance = Customer.get(id)
		customerInstance = calculationsService.getTotalPoints(customerInstance)
		[customerInstance: customerInstance]
	}
	
	def edit(Long id){
		def customerInstance = Customer.get(id)
		[customerInstance: customerInstance]
	}
	
	def update(Long id){
		def customerInstance = Customer.get(id)
		customerInstance.properties = params
		customerInstance.save()
		redirect(action: "show", id: customerInstance.id)
	}
	
	def delete(Long id){
		def customerInstance = Customer.get(id)
		customerInstance.delete()
		redirect(action: "index")
	}
}
