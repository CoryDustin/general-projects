package rewards

class InventoryController {

    def index() { 
		render "Here is a list of everything!"
	}
	
	def edit(){
		def productName = "Breakfast Blend"
		def sku = "BB01"
		[productName: productName, sku: sku]
	}
	
	def remove(){
		render "You have removed some stuffs!"
	}
	
	def list(){
		[allProducts: Product.list()]
	}
	
}
