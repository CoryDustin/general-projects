# README #

A general projects repository that consists of all my past projects including: 

* Android development projects (Currently transitioning them to this repo)
* Spring MVC utilizing Hibernate, Thymeleaf, custom RESTful API (In Progress)
* iOS development projects (In Progress)
* and whatever else I tend to think would be fun to learn/develop!

### What is this repository for? ###

* To showcase my awesome work and research I am currently working on!
* If you would like to contact me feel free to send an email at corydustingonzales@gmail.com
* My portfolio is located at [corygonzales.com](http://www.corygonzales.com) if you would like to see what else I'm up to!