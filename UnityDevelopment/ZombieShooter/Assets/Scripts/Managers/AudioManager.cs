﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	// Public field, set in the inspector we can access
	// the audio clip through the singleton instance
	public AudioClip explosionClip;

	private static AudioManager instance;
	
	void Awake()
	{
		// Save a reference to the AudioHandler component as our singleton instance
		instance = this;
	}
	
	// Instance method, this method can be accesed through the singleton instance
	public void PlayAudio(AudioClip clip)
	{
		audio.clip = clip;
		audio.Play();
	}

	public static AudioManager Instance
	{
		get{ return instance; }
	}
}
