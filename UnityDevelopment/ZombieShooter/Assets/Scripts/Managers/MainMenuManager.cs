﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	#region Public DFGUI Callback Methods
	public void OnLevelSelectButtonClick(dfControl control, dfMouseEventArgs args)
	{
		Application.LoadLevel("MainLevel");
	}

	public void OnExitGameButtonClick(dfControl control, dfMouseEventArgs args)
	{
		Application.Quit();
	}

	public void OnOptionsButtonClick(dfControl control, dfMouseEventArgs args)
	{

	}
	#endregion

}
