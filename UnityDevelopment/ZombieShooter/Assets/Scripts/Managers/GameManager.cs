﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	// There is a property binder scripts attached to this dfProgressBar
	// So when this changes the healthCount of the player as well changes from the health.js script
	public dfProgressBar healthProgressBar;

	private float currentPlayerHealth;

	private static GameManager sInstance;

	// Use this for initialization
	void Start () 
	{
		if(!sInstance)
			sInstance = this;

		currentPlayerHealth = healthProgressBar.Value;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region Public Methods
	public void DecrementHealthCount(float decrementValue)
	{
		StartCoroutine(DecreaseHealthBar(decrementValue));
	}

	public void IncrementHealthCount(float incrementValue)
	{
		StartCoroutine (IncreaseHealthBar(incrementValue));
	}
	
	#endregion

	#region Private Methods
	private IEnumerator DecreaseHealthBar(float decrementValue)
	{
		float currentValue = healthProgressBar.Value;
		float newExpectedValue = currentValue - decrementValue;

		while(currentValue > newExpectedValue)
		{
			currentValue = Mathf.Lerp(currentValue, newExpectedValue, Time.deltaTime * 2.0f);
			//newExpectedValue = currentValue - decrementValue;
			healthProgressBar.Value = currentValue;

			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator IncreaseHealthBar(float decrementValue)
	{
		float currentValue = healthProgressBar.Value;
		float newExpectedValue = currentValue + decrementValue;
		
		while(currentValue < newExpectedValue)
		{
			currentValue = Mathf.Lerp(currentValue, newExpectedValue, Time.deltaTime * 2.0f);
			//newExpectedValue = currentValue - decrementValue;
			healthProgressBar.Value = currentValue;
			
			yield return new WaitForEndOfFrame();
		}
	}
	#endregion

	#region Public Accessors
	public float PlayerHealth
	{
		get{ return currentPlayerHealth; }
		set{ currentPlayerHealth = value; }
	}

	public static GameManager Instance
	{
		get{ return sInstance; }
	}
	#endregion
}
