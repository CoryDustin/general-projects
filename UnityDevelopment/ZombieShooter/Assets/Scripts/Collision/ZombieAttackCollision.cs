﻿using UnityEngine;
using System.Collections;

public class ZombieAttackCollision : MonoBehaviour 
{

	void OnCollisionEnter(Collision other)
	{
		if(other.transform.tag == "Player")
		{
			GameManager.Instance.DecrementHealthCount(5.0f);
		}
	}

}
