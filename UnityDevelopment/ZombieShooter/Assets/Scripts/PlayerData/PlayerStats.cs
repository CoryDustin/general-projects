﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	private int health;
	private int score;

	// Use this for initialization
	void Start () {
		health = 0; 
		score = 0; 
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public int Health
	{
		get{ return health; }
		set{ health = value;}
	}

	public int Score
	{
		get{ return score; }
		set{ score = value;}
	}

	public interface IKillable
	{
		void Kill();
	}

	public interface IDamageable<T>
	{
		void Damage(T damageTaken);
	}

	public class Avatar : MonoBehaviour, IKillable, IDamageable<float>
	{
		public void Kill()
		{
			// Kill something!
		}

		public void Damage(float damageTaken)
		{
			// Do some damage!
		}
	}

}
