﻿using UnityEngine;
using System.Collections;

public class EnemySpawnerManager : MonoBehaviour {

	public GameObject enemyToSpawnPrefab;
	public float spawnFrequency = 1.5f;
	public bool spawnEnemies = true;
	public int maxSpawnCount = 10;

	private ArrayList enemyRefList;
	private float counter = 0.0f;
	private bool maxSpawnReached = false;

	// Use this for initialization
	void Start () 
	{
		enemyRefList = new ArrayList();

	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateSpawner();
	}

	private void UpdateSpawner()
	{
		if(spawnEnemies && !maxSpawnReached)
		{
			counter += Time.deltaTime;
			if(counter >= spawnFrequency)
			{
				SpawnEnemy();
				counter = 0.0f;
			}
		}
	}

	private void SpawnEnemy()
	{
		if(enemyRefList.Count < maxSpawnCount)
		{
			GameObject temp;
			temp = GameObject.Instantiate(enemyToSpawnPrefab, transform.position, transform.rotation) as GameObject;
			enemyRefList.Add(temp);
		}
		else
		{
			maxSpawnReached = true;
			Debug.LogError("MAX SPAWNING REACHED!");
		}
	}
}
