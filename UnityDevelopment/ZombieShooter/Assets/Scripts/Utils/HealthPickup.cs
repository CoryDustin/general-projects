﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

	public float healthIncreaseAmount = 25.0f;

	void OnControllerColliderHit(ControllerColliderHit other)
	{
		if(other.transform.tag == "Player")
		{
			Debug.Log("HERERER");
			GameManager.Instance.IncrementHealthCount(healthIncreaseAmount);
			Destroy(transform.parent.gameObject);
		}
	}
}
