﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(NavMeshAgent))]
[RequireComponent (typeof(Animator))]
/// <summary>
/// Zombie AI controller.
/// Attach this script to any zombie gameobject
/// that you wish to have custom zombie behavior. 
/// Once this zombie is initialized script will 
/// set its target and if one is not assigned will 
/// default to the player within the scene. 
/// </summary>
public class ZombieAIController : MonoBehaviour 
{
	public enum ZombieClass{ SlowWalker, FastWalker };

	public ZombieClass zombieType = ZombieClass.SlowWalker;	
	public Transform playerToAttack;
	public float attackDistance = 3.0f;

	private NavMeshAgent zombieNavAgent;
	private Animator zombieAnimator;
	private float animSpeed;
	private bool attacking = false;
	private bool isDead = false;
	private health healthStats;
	private float destroyCounter = 0.0f;
	private float destroyDelay = 2.0f;


	// Use this for initialization
	void Start () 
	{
		InitAI();
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		UpdateZombieAI();
	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Player")
		{
			Debug.Log ("HIT!");
		}
	}

	/// <summary>
	/// Initialize the ZombieAIController. 
	/// </summary>
	private void InitAI()
	{
		if(!playerToAttack)
			playerToAttack = GameObject.FindGameObjectWithTag("Player").transform;

		if(GetComponent<NavMeshAgent>())
			zombieNavAgent = GetComponent<NavMeshAgent>();
		else
			return;
		
		if(GetComponent<Animator>())
			zombieAnimator = GetComponent<Animator>();
		else
			return;
		
		zombieNavAgent.SetDestination(playerToAttack.position);
		
		if(zombieType == ZombieClass.SlowWalker)
		{
			zombieNavAgent.speed = 2.5f;
			zombieAnimator.speed = 1.0f;
		}
		else if(zombieType == ZombieClass.FastWalker)
		{
			zombieNavAgent.speed = 5.5f;
			zombieAnimator.speed = 1.5f;
		}

		healthStats = GetComponent<health>();
	}

	/// <summary>
	/// Update the ZombieAIController every frame
	///
	/// </summary>
	private void UpdateZombieAI()
	{
		if(!attacking && !isDead)
			zombieNavAgent.SetDestination(playerToAttack.position);

		if(!isDead)
			CheckForAttack();

		if(healthStats.GetHealth() <= 0)
		{
			isDead = true;
		}

		if(isDead)
		{
			zombieAnimator.SetBool("Dead", isDead);
			GetComponent<BoxCollider>().enabled = false;

			destroyCounter += Time.deltaTime;
			if(destroyCounter >= destroyDelay)
			{
				Destroy(gameObject);
			}
		}
	}

	/// <summary>
	/// Updates to see if we can attack an 
	/// enemy player
	/// </summary>
	private void CheckForAttack()
	{
		if(Vector3.Distance(transform.position, playerToAttack.position) <= attackDistance)
		{
			Attack();
		}
		else
		{
			attacking = false;
			zombieAnimator.SetBool("Attack", attacking);
		}
	}

	/// <summary>
	/// Attack the enemy who is within range of this 
	/// instance of ZombieAIController
	/// </summary>
	private void Attack()
	{
		attacking = true;
		zombieNavAgent.Stop();
		zombieAnimator.SetBool("Attack", attacking);
	}


}
