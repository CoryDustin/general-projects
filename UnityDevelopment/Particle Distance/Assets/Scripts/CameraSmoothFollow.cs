using UnityEngine;
using System.Collections;

public class CameraSmoothFollow : MonoBehaviour {

	public Transform target;
	public float distanceFromPlayer = 10.0f;
	public float height = 4.0f;
	public float xLocation = 10.0f;
	
	private bool facingRight = true;
	private Vector3	pos;
	
	private Vector3 initialCamPos;
	
	
	// Use this for initialization
	void Start () {
		//See below notes on what these variable positions mean.
		initialCamPos = transform.position;
		initialCamPos.z = target.position.z - distanceFromPlayer;
		pos = new Vector3( (transform.position.x + xLocation), (transform.position.y + height), transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Horizontal") > 0)  //Pressed right key or d key.
			facingRight = true;
		if(Input.GetAxis("Horizontal") < 0)  //Moving left instead.
			facingRight = false;
		
		//X in vector is horizontal position in regards to target ie 1 moves more to right.
		//Y postion in vector increases height of the camera based off the heights location of target.
		//z position is zooming in or out towards the target.  Negative zooms out but producers will want positive most likely.
			
		if(facingRight)
			pos = new Vector3( (target.position.x + xLocation), (target.position.y + height), initialCamPos.z);
	
		else if(facingRight == false)
			pos = new Vector3( (target.position.x + -xLocation), (target.position.y + height), initialCamPos.z);
		
		transform.position = pos;
	}

}
