using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	
	CharacterController controller; 
	
	public bool depth = true;
	public float speed = 6.0f;
	public float jetSpeed = 8.0f;
	public int maxJetSpeed = 250;
	public float gravity = 20.0f;
	
	//public ParticleEmitter jetParticles;
	//public GameObject jetParticleSpawn1;
	//public GameObject jetParticleSpawn2;
	
	public bool rotateWhileJump = true;
	
	public bool isRotationFast = false;
	public float rotationSpeed = 2.0f;
	
	//private PlayerStats playerStats;
	
	public Vector3 angle;
	public RaycastHit hit;
	private float rayDistance;
	
	private float rotationXAxis = 0.0f;
	private float rotationYAxis = 0.0f;
	private float rotationZAxis = 0.0f;

	private Quaternion rotate;
	
	public bool playerHasControl = true;

	public Vector3 moveDirection = Vector3.zero;
	
	// Use this for initialization
	void Start () {

		rotationYAxis = 90.0f;
		
		//playerStats = gameObject.GetComponent<PlayerStats>();
		
	}
	
	void Update () {
		
		if(playerHasControl == true)
		{
			//The object that gets this script MUST have a CharacterController on it.
			//It cannot have a rigid body on it and cannot have a type of mesh collider.
			controller = GetComponent<CharacterController>();
			
			if(controller.isGrounded)
			{
				moveDirection = new Vector3(0, 0, 0);
		
				moveDirection *= speed;
		
				
			}
			
			if (Input.GetButton("Jump") || Input.GetKeyDown(KeyCode.Joystick1Button0))
			{	
				moveDirection.y += jetSpeed;
				
				if(moveDirection.y >= maxJetSpeed)
				{
					moveDirection.y -= maxJetSpeed;
				}
				
				print("moveDirection: " + moveDirection.y);
				
			}
									
			moveDirection.y -= gravity * Time.deltaTime;
		
			//These next 4 lines are copied here to allow changing movement mid air
//			if( (Input.GetAxis("Horizontal") > 0) || (Input.GetAxis("Horizontal") < 0) )
//			{
			moveDirection.z = Input.GetAxis("Horizontal") * speed;
//			}
			if( (Input.GetAxis("Horizontal") > 0) || (Input.GetAxis("Horizontal") < 0) )
				moveDirection.z = Input.GetAxis("Horizontal") * speed;
		
			controller.Move(moveDirection * Time.deltaTime);
		
		}
	}	

}
